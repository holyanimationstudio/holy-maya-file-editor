#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date     : 3/7/2017 5:40 PM
# @Author   : Godfrey Huang

import logging

from widget_handler import WidgetHandler

def getLogger(name):
    """Get's a logger and attaches the correct DCC compatible Handler.

    Args:
        name (str): Name of the logger to get / create.

    Returns:
        Logger: Logger.

    """

    logger = logging.getLogger(name)

    handlerNames = [type(x).__name__ for x in logger.handlers]
    if 'WidgetHandler' not in handlerNames:
        widgetHandler = WidgetHandler()
        if widgetHandler is not None:
            logger.addHandler(widgetHandler)

    return logger