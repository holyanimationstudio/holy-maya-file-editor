#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date     : 3/1/2018 10:15 AM
# @Author   : Godfrey Huang

import os
import time
from upUtil import uploadUtil, Get

pathGetter = Get.Get()

class Uploader(uploadUtil.uploadUtil):
    def __init__(self,localDir,uploadDir,files=[],*args):
        super(Uploader,self).__init__(skipMainFile=False)
        self.localDir = localDir # C:\Users\qiu.huang\AppData\Local\Temp\upload20180626154653
        self.uploadDir = uploadDir # M:\TD&RD\qiu.huang\nezhe_output
        self.files = files

    def _upInit(self):
        pass

    def do(self):
        # serverVerDir = self.uploadDir+'/Main.v001'
        backupBaseDir = pathGetter.Get_BackupPath()+'/NEZHA/N'
        if not os.path.isdir(self.localDir) or not self.uploadDir or not self.files:
            return

        for filePath in self.files:
            filePath = filePath.replace('\\','/')
            localFilePath = self.localDir + filePath
            serverFilePath = self.uploadDir + filePath
            backupDir = backupBaseDir + os.path.dirname(filePath)
            self.addToPkg('extendedFiles',  #标记为非主要文件
                        localFilePath,      #要上传的本地文件
                        serverFilePath,     #上传到服务器位置
                        #symlinkPath=serverLinkFilePath,   #服务器版本链接的位置
                        deleteLocal=True,   #上传后是否删除本地文件
                        backupDir=backupDir
                        )

        # timestamps = time.strftime('%Y%m%d%H%M%S',time.localtime(time.time()))
        packageFilePath = self.localDir + '/replaceAttributePackage.json'
        self.finalizePkg(1.0,packageFilePath,
                        self.uploadDir + '/replaceAttributePackage.json',
                        pkgServerVerPath=self.uploadDir + '/replaceAttributePackage.json',
                        )

        self._uploadFileList()