# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\codes\dev\holy-maya-file-editor\holyMayaFileEditor\ui\mainWindow.ui'
#
# Created: Wed Oct 24 15:28:21 2018
#      by: pyside2-uic  running on PySide2 5.6.0a1
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(826, 597)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.directory_label = QtWidgets.QLabel(self.centralwidget)
        self.directory_label.setMinimumSize(QtCore.QSize(100, 0))
        self.directory_label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.directory_label.setObjectName("directory_label")
        self.horizontalLayout.addWidget(self.directory_label)
        self.directory_comboBox = QtWidgets.QComboBox(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.directory_comboBox.sizePolicy().hasHeightForWidth())
        self.directory_comboBox.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.directory_comboBox.setFont(font)
        self.directory_comboBox.setObjectName("directory_comboBox")
        self.horizontalLayout.addWidget(self.directory_comboBox)
        self.browse_pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.browse_pushButton.setMinimumSize(QtCore.QSize(60, 0))
        self.browse_pushButton.setObjectName("browse_pushButton")
        self.horizontalLayout.addWidget(self.browse_pushButton)
        self.horizontalLayout.setStretch(1, 1)
        self.verticalLayout_3.addLayout(self.horizontalLayout)
        self.splitter = QtWidgets.QSplitter(self.centralwidget)
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName("splitter")
        self.layoutWidget = QtWidgets.QWidget(self.splitter)
        self.layoutWidget.setObjectName("layoutWidget")
        self.source_files_verticalLayout = QtWidgets.QVBoxLayout(self.layoutWidget)
        self.source_files_verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.source_files_verticalLayout.setObjectName("source_files_verticalLayout")
        self.source_regular_horizontalLayout = QtWidgets.QHBoxLayout()
        self.source_regular_horizontalLayout.setObjectName("source_regular_horizontalLayout")
        self.source_regular_label = QtWidgets.QLabel(self.layoutWidget)
        self.source_regular_label.setMinimumSize(QtCore.QSize(100, 0))
        self.source_regular_label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.source_regular_label.setObjectName("source_regular_label")
        self.source_regular_horizontalLayout.addWidget(self.source_regular_label)
        self.source_regular_comboBox = QtWidgets.QComboBox(self.layoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.source_regular_comboBox.setFont(font)
        self.source_regular_comboBox.setEditable(True)
        self.source_regular_comboBox.setFrame(True)
        self.source_regular_comboBox.setObjectName("source_regular_comboBox")
        self.source_regular_comboBox.addItem("")
        self.source_regular_horizontalLayout.addWidget(self.source_regular_comboBox)
        self.source_search_pushButton = QtWidgets.QPushButton(self.layoutWidget)
        self.source_search_pushButton.setMinimumSize(QtCore.QSize(60, 0))
        self.source_search_pushButton.setObjectName("source_search_pushButton")
        self.source_regular_horizontalLayout.addWidget(self.source_search_pushButton)
        self.source_regular_horizontalLayout.setStretch(1, 1)
        self.source_files_verticalLayout.addLayout(self.source_regular_horizontalLayout)
        self.line = QtWidgets.QFrame(self.layoutWidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.source_files_verticalLayout.addWidget(self.line)
        self.source_filed_widget = QtWidgets.QWidget(self.layoutWidget)
        self.source_filed_widget.setAutoFillBackground(True)
        self.source_filed_widget.setStyleSheet("")
        self.source_filed_widget.setObjectName("source_filed_widget")
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout(self.source_filed_widget)
        self.horizontalLayout_6.setSpacing(6)
        self.horizontalLayout_6.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem)
        self.horizontalLayout_8 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_8.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        self.label_2 = QtWidgets.QLabel(self.source_filed_widget)
        font = QtGui.QFont()
        font.setPointSize(7)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_8.addWidget(self.label_2)
        self.source_sort_comboBox = QtWidgets.QComboBox(self.source_filed_widget)
        font = QtGui.QFont()
        font.setPointSize(7)
        self.source_sort_comboBox.setFont(font)
        self.source_sort_comboBox.setObjectName("source_sort_comboBox")
        self.source_sort_comboBox.addItem("")
        self.source_sort_comboBox.addItem("")
        self.source_sort_comboBox.addItem("")
        self.source_sort_comboBox.addItem("")
        self.horizontalLayout_8.addWidget(self.source_sort_comboBox)
        self.horizontalLayout_6.addLayout(self.horizontalLayout_8)
        self.source_fileds_toolButton = QtWidgets.QToolButton(self.source_filed_widget)
        self.source_fileds_toolButton.setMinimumSize(QtCore.QSize(56, 24))
        font = QtGui.QFont()
        font.setPointSize(7)
        self.source_fileds_toolButton.setFont(font)
        self.source_fileds_toolButton.setPopupMode(QtWidgets.QToolButton.InstantPopup)
        self.source_fileds_toolButton.setAutoRaise(False)
        self.source_fileds_toolButton.setArrowType(QtCore.Qt.NoArrow)
        self.source_fileds_toolButton.setObjectName("source_fileds_toolButton")
        self.horizontalLayout_6.addWidget(self.source_fileds_toolButton)
        self.source_files_verticalLayout.addWidget(self.source_filed_widget)
        self.source_files_treeView = QtWidgets.QTreeView(self.layoutWidget)
        self.source_files_treeView.setObjectName("source_files_treeView")
        self.source_files_verticalLayout.addWidget(self.source_files_treeView)
        self.horizontalLayout_10 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_10.setObjectName("horizontalLayout_10")
        self.source_items_label = QtWidgets.QLabel(self.layoutWidget)
        self.source_items_label.setObjectName("source_items_label")
        self.horizontalLayout_10.addWidget(self.source_items_label)
        self.source_select_state_label = QtWidgets.QLabel(self.layoutWidget)
        self.source_select_state_label.setText("")
        self.source_select_state_label.setObjectName("source_select_state_label")
        self.horizontalLayout_10.addWidget(self.source_select_state_label)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_10.addItem(spacerItem1)
        self.source_files_verticalLayout.addLayout(self.horizontalLayout_10)
        self.layoutWidget_2 = QtWidgets.QWidget(self.splitter)
        self.layoutWidget_2.setObjectName("layoutWidget_2")
        self.target_files_verticalLayout = QtWidgets.QVBoxLayout(self.layoutWidget_2)
        self.target_files_verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.target_files_verticalLayout.setObjectName("target_files_verticalLayout")
        self.target_regular_horizontalLayout = QtWidgets.QHBoxLayout()
        self.target_regular_horizontalLayout.setObjectName("target_regular_horizontalLayout")
        self.target_regular_label = QtWidgets.QLabel(self.layoutWidget_2)
        self.target_regular_label.setMinimumSize(QtCore.QSize(100, 0))
        self.target_regular_label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.target_regular_label.setObjectName("target_regular_label")
        self.target_regular_horizontalLayout.addWidget(self.target_regular_label)
        self.target_regular_comboBox = QtWidgets.QComboBox(self.layoutWidget_2)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.target_regular_comboBox.setFont(font)
        self.target_regular_comboBox.setEditable(True)
        self.target_regular_comboBox.setFrame(True)
        self.target_regular_comboBox.setObjectName("target_regular_comboBox")
        self.target_regular_comboBox.addItem("")
        self.target_regular_horizontalLayout.addWidget(self.target_regular_comboBox)
        self.target_filter_pushButton = QtWidgets.QPushButton(self.layoutWidget_2)
        self.target_filter_pushButton.setMinimumSize(QtCore.QSize(60, 0))
        self.target_filter_pushButton.setObjectName("target_filter_pushButton")
        self.target_regular_horizontalLayout.addWidget(self.target_filter_pushButton)
        self.target_regular_horizontalLayout.setStretch(1, 1)
        self.target_files_verticalLayout.addLayout(self.target_regular_horizontalLayout)
        self.line_2 = QtWidgets.QFrame(self.layoutWidget_2)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.target_files_verticalLayout.addWidget(self.line_2)
        self.target_filed_widget = QtWidgets.QWidget(self.layoutWidget_2)
        self.target_filed_widget.setObjectName("target_filed_widget")
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout(self.target_filed_widget)
        self.horizontalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_7.addItem(spacerItem2)
        self.horizontalLayout_9 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_9.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_9.setObjectName("horizontalLayout_9")
        self.label_3 = QtWidgets.QLabel(self.target_filed_widget)
        font = QtGui.QFont()
        font.setPointSize(7)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_9.addWidget(self.label_3)
        self.target_sort_comboBox = QtWidgets.QComboBox(self.target_filed_widget)
        font = QtGui.QFont()
        font.setPointSize(7)
        self.target_sort_comboBox.setFont(font)
        self.target_sort_comboBox.setObjectName("target_sort_comboBox")
        self.target_sort_comboBox.addItem("")
        self.target_sort_comboBox.addItem("")
        self.target_sort_comboBox.addItem("")
        self.target_sort_comboBox.addItem("")
        self.horizontalLayout_9.addWidget(self.target_sort_comboBox)
        self.horizontalLayout_7.addLayout(self.horizontalLayout_9)
        self.target_fileds_toolButton = QtWidgets.QToolButton(self.target_filed_widget)
        self.target_fileds_toolButton.setMinimumSize(QtCore.QSize(56, 24))
        font = QtGui.QFont()
        font.setPointSize(7)
        self.target_fileds_toolButton.setFont(font)
        self.target_fileds_toolButton.setPopupMode(QtWidgets.QToolButton.InstantPopup)
        self.target_fileds_toolButton.setAutoRaise(False)
        self.target_fileds_toolButton.setObjectName("target_fileds_toolButton")
        self.horizontalLayout_7.addWidget(self.target_fileds_toolButton)
        self.target_files_verticalLayout.addWidget(self.target_filed_widget)
        self.target_files_treeView = QtWidgets.QTreeView(self.layoutWidget_2)
        self.target_files_treeView.setObjectName("target_files_treeView")
        self.target_files_verticalLayout.addWidget(self.target_files_treeView)
        self.horizontalLayout_11 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_11.setObjectName("horizontalLayout_11")
        self.target_items_label = QtWidgets.QLabel(self.layoutWidget_2)
        self.target_items_label.setObjectName("target_items_label")
        self.horizontalLayout_11.addWidget(self.target_items_label)
        self.target_select_state_label = QtWidgets.QLabel(self.layoutWidget_2)
        self.target_select_state_label.setText("")
        self.target_select_state_label.setObjectName("target_select_state_label")
        self.horizontalLayout_11.addWidget(self.target_select_state_label)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_11.addItem(spacerItem3)
        self.target_files_verticalLayout.addLayout(self.horizontalLayout_11)
        self.verticalLayout_3.addWidget(self.splitter)
        self.line_3 = QtWidgets.QFrame(self.centralwidget)
        self.line_3.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.verticalLayout_3.addWidget(self.line_3)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.horizontalLayout_14 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_14.setObjectName("horizontalLayout_14")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_14.addWidget(self.label_4)
        self.level_comboBox = QtWidgets.QComboBox(self.centralwidget)
        self.level_comboBox.setObjectName("level_comboBox")
        self.level_comboBox.addItem("")
        self.level_comboBox.addItem("")
        self.level_comboBox.addItem("")
        self.horizontalLayout_14.addWidget(self.level_comboBox)
        self.horizontalLayout_3.addLayout(self.horizontalLayout_14)
        self.traverse_folder_checkBox = QtWidgets.QCheckBox(self.centralwidget)
        self.traverse_folder_checkBox.setObjectName("traverse_folder_checkBox")
        self.horizontalLayout_3.addWidget(self.traverse_folder_checkBox)
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem4)
        self.verticalLayout_3.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setMinimumSize(QtCore.QSize(100, 0))
        self.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label.setObjectName("label")
        self.horizontalLayout_2.addWidget(self.label)
        self.callback_comboBox = QtWidgets.QComboBox(self.centralwidget)
        self.callback_comboBox.setEnabled(True)
        self.callback_comboBox.setObjectName("callback_comboBox")
        self.callback_comboBox.addItem("")
        self.callback_comboBox.addItem("")
        self.callback_comboBox.addItem("")
        self.callback_comboBox.addItem("")
        self.horizontalLayout_2.addWidget(self.callback_comboBox)
        self.callback_tip_info_label = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.callback_tip_info_label.setFont(font)
        self.callback_tip_info_label.setText("")
        self.callback_tip_info_label.setObjectName("callback_tip_info_label")
        self.horizontalLayout_2.addWidget(self.callback_tip_info_label)
        spacerItem5 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem5)
        self.verticalLayout_3.addLayout(self.horizontalLayout_2)
        self.output_directory_widget = QtWidgets.QWidget(self.centralwidget)
        self.output_directory_widget.setEnabled(True)
        self.output_directory_widget.setObjectName("output_directory_widget")
        self.horizontalLayout_12 = QtWidgets.QHBoxLayout(self.output_directory_widget)
        self.horizontalLayout_12.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_12.setObjectName("horizontalLayout_12")
        self.output_directory_label = QtWidgets.QLabel(self.output_directory_widget)
        self.output_directory_label.setMinimumSize(QtCore.QSize(100, 0))
        self.output_directory_label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.output_directory_label.setObjectName("output_directory_label")
        self.horizontalLayout_12.addWidget(self.output_directory_label)
        self.output_directory_comboBox = QtWidgets.QComboBox(self.output_directory_widget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.output_directory_comboBox.sizePolicy().hasHeightForWidth())
        self.output_directory_comboBox.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.output_directory_comboBox.setFont(font)
        self.output_directory_comboBox.setObjectName("output_directory_comboBox")
        self.horizontalLayout_12.addWidget(self.output_directory_comboBox)
        self.output_directory_browse_pushButton = QtWidgets.QPushButton(self.output_directory_widget)
        self.output_directory_browse_pushButton.setMinimumSize(QtCore.QSize(60, 0))
        self.output_directory_browse_pushButton.setObjectName("output_directory_browse_pushButton")
        self.horizontalLayout_12.addWidget(self.output_directory_browse_pushButton)
        self.horizontalLayout_12.setStretch(1, 1)
        self.verticalLayout_3.addWidget(self.output_directory_widget)
        self.replace_attribute_mode_widget = QtWidgets.QWidget(self.centralwidget)
        self.replace_attribute_mode_widget.setMinimumSize(QtCore.QSize(0, 0))
        self.replace_attribute_mode_widget.setObjectName("replace_attribute_mode_widget")
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout(self.replace_attribute_mode_widget)
        self.horizontalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.replace_attribute_label = QtWidgets.QLabel(self.replace_attribute_mode_widget)
        self.replace_attribute_label.setMinimumSize(QtCore.QSize(100, 0))
        self.replace_attribute_label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.replace_attribute_label.setObjectName("replace_attribute_label")
        self.horizontalLayout_5.addWidget(self.replace_attribute_label)
        self.replace_attribute_mode_comboBox = QtWidgets.QComboBox(self.replace_attribute_mode_widget)
        self.replace_attribute_mode_comboBox.setObjectName("replace_attribute_mode_comboBox")
        self.replace_attribute_mode_comboBox.addItem("")
        self.replace_attribute_mode_comboBox.addItem("")
        self.horizontalLayout_5.addWidget(self.replace_attribute_mode_comboBox)
        spacerItem6 = QtWidgets.QSpacerItem(560, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem6)
        self.horizontalLayout_5.setStretch(1, 1)
        self.verticalLayout_3.addWidget(self.replace_attribute_mode_widget)
        self.horizontalLayout_13 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_13.setObjectName("horizontalLayout_13")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setMinimumSize(QtCore.QSize(0, 0))
        self.label_5.setMaximumSize(QtCore.QSize(100, 16777215))
        self.label_5.setText("")
        self.label_5.setObjectName("label_5")
        self.horizontalLayout_13.addWidget(self.label_5)
        self.upload_checkBox = QtWidgets.QCheckBox(self.centralwidget)
        self.upload_checkBox.setObjectName("upload_checkBox")
        self.horizontalLayout_13.addWidget(self.upload_checkBox)
        self.verticalLayout_3.addLayout(self.horizontalLayout_13)
        self.output_json_setting_widget = QtWidgets.QWidget(self.centralwidget)
        self.output_json_setting_widget.setObjectName("output_json_setting_widget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.output_json_setting_widget)
        self.verticalLayout.setContentsMargins(0, -1, -1, -1)
        self.verticalLayout.setObjectName("verticalLayout")
        self.output_json_file_horizontalLayout = QtWidgets.QHBoxLayout()
        self.output_json_file_horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.output_json_file_horizontalLayout.setObjectName("output_json_file_horizontalLayout")
        self.label_7 = QtWidgets.QLabel(self.output_json_setting_widget)
        self.label_7.setMinimumSize(QtCore.QSize(100, 0))
        self.label_7.setText("")
        self.label_7.setObjectName("label_7")
        self.output_json_file_horizontalLayout.addWidget(self.label_7)
        self.output_json_file_checkBox = QtWidgets.QCheckBox(self.output_json_setting_widget)
        self.output_json_file_checkBox.setObjectName("output_json_file_checkBox")
        self.output_json_file_horizontalLayout.addWidget(self.output_json_file_checkBox)
        self.output_json_file_horizontalLayout.setStretch(1, 1)
        self.verticalLayout.addLayout(self.output_json_file_horizontalLayout)
        self.output_json_output_directory_widget = QtWidgets.QWidget(self.output_json_setting_widget)
        self.output_json_output_directory_widget.setObjectName("output_json_output_directory_widget")
        self.horizontalLayout_16 = QtWidgets.QHBoxLayout(self.output_json_output_directory_widget)
        self.horizontalLayout_16.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_16.setObjectName("horizontalLayout_16")
        self.label_9 = QtWidgets.QLabel(self.output_json_output_directory_widget)
        self.label_9.setMinimumSize(QtCore.QSize(100, 0))
        self.label_9.setObjectName("label_9")
        self.horizontalLayout_16.addWidget(self.label_9)
        self.output_json_output_directory_lineEdit = QtWidgets.QLineEdit(self.output_json_output_directory_widget)
        self.output_json_output_directory_lineEdit.setObjectName("output_json_output_directory_lineEdit")
        self.horizontalLayout_16.addWidget(self.output_json_output_directory_lineEdit)
        self.horizontalLayout_16.setStretch(1, 1)
        self.verticalLayout.addWidget(self.output_json_output_directory_widget)
        self.output_json_regular_widget = QtWidgets.QWidget(self.output_json_setting_widget)
        self.output_json_regular_widget.setObjectName("output_json_regular_widget")
        self.horizontalLayout_15 = QtWidgets.QHBoxLayout(self.output_json_regular_widget)
        self.horizontalLayout_15.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_15.setObjectName("horizontalLayout_15")
        self.label_6 = QtWidgets.QLabel(self.output_json_regular_widget)
        self.label_6.setMinimumSize(QtCore.QSize(100, 0))
        self.label_6.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_6.setObjectName("label_6")
        self.horizontalLayout_15.addWidget(self.label_6)
        self.output_json_regular_source_lineEdit = QtWidgets.QLineEdit(self.output_json_regular_widget)
        self.output_json_regular_source_lineEdit.setObjectName("output_json_regular_source_lineEdit")
        self.horizontalLayout_15.addWidget(self.output_json_regular_source_lineEdit)
        self.label_8 = QtWidgets.QLabel(self.output_json_regular_widget)
        self.label_8.setObjectName("label_8")
        self.horizontalLayout_15.addWidget(self.label_8)
        self.output_json_regular_target_lineEdit = QtWidgets.QLineEdit(self.output_json_regular_widget)
        self.output_json_regular_target_lineEdit.setObjectName("output_json_regular_target_lineEdit")
        self.horizontalLayout_15.addWidget(self.output_json_regular_target_lineEdit)
        self.verticalLayout.addWidget(self.output_json_regular_widget)
        self.verticalLayout_3.addWidget(self.output_json_setting_widget)
        self.line_4 = QtWidgets.QFrame(self.centralwidget)
        self.line_4.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_4.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_4.setObjectName("line_4")
        self.verticalLayout_3.addWidget(self.line_4)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.log_pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.log_pushButton.setMinimumSize(QtCore.QSize(120, 36))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.log_pushButton.setFont(font)
        self.log_pushButton.setObjectName("log_pushButton")
        self.horizontalLayout_4.addWidget(self.log_pushButton)
        spacerItem7 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem7)
        self.batch_pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.batch_pushButton.setMinimumSize(QtCore.QSize(200, 36))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setWeight(75)
        font.setBold(True)
        self.batch_pushButton.setFont(font)
        self.batch_pushButton.setStyleSheet("background-color: rgba(34, 188, 253, 128);")
        self.batch_pushButton.setObjectName("batch_pushButton")
        self.horizontalLayout_4.addWidget(self.batch_pushButton)
        self.close_pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.close_pushButton.setMinimumSize(QtCore.QSize(120, 36))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.close_pushButton.setFont(font)
        self.close_pushButton.setObjectName("close_pushButton")
        self.horizontalLayout_4.addWidget(self.close_pushButton)
        self.verticalLayout_3.addLayout(self.horizontalLayout_4)
        self.verticalLayout_3.setStretch(1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 826, 21))
        self.menubar.setObjectName("menubar")
        self.menuHelp = QtWidgets.QMenu(self.menubar)
        self.menuHelp.setObjectName("menuHelp")
        MainWindow.setMenuBar(self.menubar)
        self.action = QtWidgets.QAction(MainWindow)
        self.action.setText("")
        self.action.setObjectName("action")
        self.actionAbout = QtWidgets.QAction(MainWindow)
        self.actionAbout.setObjectName("actionAbout")
        self.menuHelp.addAction(self.actionAbout)
        self.menubar.addAction(self.menuHelp.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QObject.connect(self.close_pushButton, QtCore.SIGNAL("clicked()"), MainWindow.close)
        QtCore.QObject.connect(self.output_json_file_checkBox, QtCore.SIGNAL("toggled(bool)"), self.output_json_regular_widget.setVisible)
        QtCore.QObject.connect(self.output_json_file_checkBox, QtCore.SIGNAL("clicked(bool)"), self.output_json_output_directory_widget.setVisible)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtWidgets.QApplication.translate("MainWindow", "Maya File Editor", None, -1))
        self.directory_label.setText(QtWidgets.QApplication.translate("MainWindow", "Directory:", None, -1))
        self.browse_pushButton.setText(QtWidgets.QApplication.translate("MainWindow", "&Browse..", None, -1))
        self.source_regular_label.setText(QtWidgets.QApplication.translate("MainWindow", "Source Regular:", None, -1))
        self.source_regular_comboBox.setItemText(0, QtWidgets.QApplication.translate("MainWindow", "[a-zA-Z0-9_]*", None, -1))
        self.source_search_pushButton.setText(QtWidgets.QApplication.translate("MainWindow", "Search", None, -1))
        self.label_2.setText(QtWidgets.QApplication.translate("MainWindow", "Sort:", None, -1))
        self.source_sort_comboBox.setItemText(0, QtWidgets.QApplication.translate("MainWindow", "A-Z", None, -1))
        self.source_sort_comboBox.setItemText(1, QtWidgets.QApplication.translate("MainWindow", "Z-A", None, -1))
        self.source_sort_comboBox.setItemText(2, QtWidgets.QApplication.translate("MainWindow", "Latest", None, -1))
        self.source_sort_comboBox.setItemText(3, QtWidgets.QApplication.translate("MainWindow", "Oldest", None, -1))
        self.source_fileds_toolButton.setText(QtWidgets.QApplication.translate("MainWindow", "Fields", None, -1))
        self.source_items_label.setText(QtWidgets.QApplication.translate("MainWindow", "0 items", None, -1))
        self.target_regular_label.setText(QtWidgets.QApplication.translate("MainWindow", "Target Regular:", None, -1))
        self.target_regular_comboBox.setItemText(0, QtWidgets.QApplication.translate("MainWindow", "[a-zA-Z0-9_]*", None, -1))
        self.target_filter_pushButton.setText(QtWidgets.QApplication.translate("MainWindow", "Filter", None, -1))
        self.label_3.setText(QtWidgets.QApplication.translate("MainWindow", "Sort:", None, -1))
        self.target_sort_comboBox.setItemText(0, QtWidgets.QApplication.translate("MainWindow", "A-Z", None, -1))
        self.target_sort_comboBox.setItemText(1, QtWidgets.QApplication.translate("MainWindow", "Z-A", None, -1))
        self.target_sort_comboBox.setItemText(2, QtWidgets.QApplication.translate("MainWindow", "Latest", None, -1))
        self.target_sort_comboBox.setItemText(3, QtWidgets.QApplication.translate("MainWindow", "Oldest", None, -1))
        self.target_fileds_toolButton.setText(QtWidgets.QApplication.translate("MainWindow", "Fields", None, -1))
        self.target_items_label.setText(QtWidgets.QApplication.translate("MainWindow", "0 items", None, -1))
        self.label_4.setText(QtWidgets.QApplication.translate("MainWindow", "Level:", None, -1))
        self.level_comboBox.setItemText(0, QtWidgets.QApplication.translate("MainWindow", "0", None, -1))
        self.level_comboBox.setItemText(1, QtWidgets.QApplication.translate("MainWindow", "1", None, -1))
        self.level_comboBox.setItemText(2, QtWidgets.QApplication.translate("MainWindow", "all", None, -1))
        self.traverse_folder_checkBox.setText(QtWidgets.QApplication.translate("MainWindow", "Traverse folder", None, -1))
        self.label.setText(QtWidgets.QApplication.translate("MainWindow", "Callback:", None, -1))
        self.callback_comboBox.setItemText(0, QtWidgets.QApplication.translate("MainWindow", "None", None, -1))
        self.callback_comboBox.setItemText(1, QtWidgets.QApplication.translate("MainWindow", "output material", None, -1))
        self.callback_comboBox.setItemText(2, QtWidgets.QApplication.translate("MainWindow", "replace attribute", None, -1))
        self.callback_comboBox.setItemText(3, QtWidgets.QApplication.translate("MainWindow", "sequence check and copy", None, -1))
        self.output_directory_label.setText(QtWidgets.QApplication.translate("MainWindow", "Output Directory:", None, -1))
        self.output_directory_browse_pushButton.setText(QtWidgets.QApplication.translate("MainWindow", "&Browse..", None, -1))
        self.replace_attribute_label.setText(QtWidgets.QApplication.translate("MainWindow", "Mode:", None, -1))
        self.replace_attribute_mode_comboBox.setItemText(0, QtWidgets.QApplication.translate("MainWindow", "${cocoN} --> N:", None, -1))
        self.replace_attribute_mode_comboBox.setItemText(1, QtWidgets.QApplication.translate("MainWindow", "N: --> ${cocoN}", None, -1))
        self.upload_checkBox.setText(QtWidgets.QApplication.translate("MainWindow", "Use the pipeline to upload and backup", None, -1))
        self.output_json_file_checkBox.setText(QtWidgets.QApplication.translate("MainWindow", "Output Json File", None, -1))
        self.label_9.setText(QtWidgets.QApplication.translate("MainWindow", "Output Directory:", None, -1))
        self.output_json_output_directory_lineEdit.setPlaceholderText(QtWidgets.QApplication.translate("MainWindow", "relative path, such as: ../..", None, -1))
        self.label_6.setText(QtWidgets.QApplication.translate("MainWindow", "Replace:", None, -1))
        self.output_json_regular_source_lineEdit.setPlaceholderText(QtWidgets.QApplication.translate("MainWindow", "souce text", None, -1))
        self.label_8.setText(QtWidgets.QApplication.translate("MainWindow", "-->", None, -1))
        self.output_json_regular_target_lineEdit.setPlaceholderText(QtWidgets.QApplication.translate("MainWindow", "target text", None, -1))
        self.log_pushButton.setText(QtWidgets.QApplication.translate("MainWindow", "Log", None, -1))
        self.batch_pushButton.setText(QtWidgets.QApplication.translate("MainWindow", "Batch", None, -1))
        self.close_pushButton.setText(QtWidgets.QApplication.translate("MainWindow", "Close", None, -1))
        self.menuHelp.setTitle(QtWidgets.QApplication.translate("MainWindow", "Help", None, -1))
        self.actionAbout.setText(QtWidgets.QApplication.translate("MainWindow", "About", None, -1))

