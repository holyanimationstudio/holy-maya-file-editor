#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date     : 3/1/2018 10:15 AM
# @Author   : Godfrey Huang

import os
import time
import re
import subprocess
import codecs
from Qt import QtWidgets, QtCore, QtGui

import pymel.core as pm
import maya.cmds as cmds
import maya.mel as mel

import logging
from log import getLogger

logger = getLogger('holyMayaFileEditor')
logger.setLevel(logging.DEBUG)

class Core:
    MAYA = 'maya'
    EEXPLORER = 'explorer'
    RVPLAYER = 'rv'
    COLOR_BG_RED = (255,0,0,50)

    def __init__(self):
        pass

    @staticmethod
    def openInApp(app,model,indexes,*args):
        for index in indexes:
            filePath = model.filePath(index)
            filePath = filePath.replace('/','\\')
            if app == Core.MAYA:
                cmds.file(f=True,new=True)
                cmds.file(filePath,f=True,options="v=0;",ignoreVersion=True,o=True)
            elif app == Core.EEXPLORER:
                subprocess.Popen(r'explorer /select, "%s"' % filePath)
            elif app == Core.RVPLAYER:
                from upUtil import ProjectConfig
                RVPath = ProjectConfig.ProjectConfig().getValue('General/RVPath',default=None)
                RVPATH = RVPath.replace('/','\\')
                backgroundColor = index.data(QtCore.Qt.BackgroundColorRole)
                backgroundColor = backgroundColor.getRgb() if backgroundColor else None
                if backgroundColor:
                    if backgroundColor==Core.COLOR_BG_RED:
                        logger.warning('No exr file detected, cannot be imported into RV player!')
                else:
                    subprocess.Popen(r'%s "%s"' % (RVPath, filePath))

    @staticmethod
    def deleteRequirePlugin(sourceFilePath=None,targetFilePath=None,*args):
        startTime = time.time()
        if os.path.isfile(sourceFilePath):
            with open(sourceFilePath,'r') as sourceFile:
                with open(targetFilePath,'w') as targetFile:
                    line = sourceFile.readline()
                    requiresPattern = re.compile('requires[\s\S]+')
                    requirePattern = re.compile('requires[\s\S]+;')
                    requireVersionPattern = re.compile('requires maya "[0-9]+";')
                    endCharPattern = re.compile(r';')
                    state = True
                    while line:
                        if (re.findall(requiresPattern,line) and not re.findall(endCharPattern,line)):
                            line = line.strip() + ' ' + sourceFile.readline().strip()
                            continue

                        if re.findall(requireVersionPattern,line) or not re.findall(requirePattern,line):
                            targetFile.write(line)
                        line = sourceFile.readline()
        endTime = time.time()
        logger.debug(format('Remove resources plugin line, saved file.',' <120')+'%.2f s' % (endTime-startTime))

    @staticmethod
    def deleteAlembicNodes():
        alembicNodes = pm.ls(type='AlembicNode')
        if alembicNodes:
            for aNode in alembicNodes:
                pm.delete(aNode)
            logger.debug('Deleted %i Alembic Nodes.' % len(alembicNodes))

    @staticmethod
    def deleteNotUseFaceOfMeshs():
        startTime = time.time()
        meshes = pm.ls(type='mesh')
        if meshes:
            node = meshes[0]
            rootNode = node.root()
            rootNode.visibility.unlock()
            rootNode.visibility.set(0)
            sgNodes = pm.ls(type='shadingEngine')
            if sgNodes:
                shaders = list()
                for i in range(len(sgNodes)):
                    sgNode = sgNodes[i]
                    shaderMats = sgNode.ss.inputs()
                    shaderMat = shaderMats[0] if shaderMats else None
                    if shaderMat and (not shaderMat in shaders): shaders.append(shaderMat)
                for i in range(len(shaders)):
                    pm.hyperShade(objects=shaders[i])
                    sels = pm.ls(sl=True)
                    if sels and len(sels)>0:
                        if len(sels) > 1:
                            pm.delete(sels[1:])
                        sel = sels[0]
                        if str(type(sel)).find('MeshFace') != -1:
                            pattern = re.compile('[a-zA-Z0-9_]+.f\[([0-9]+)\:([0-9]+)\]')
                            buf = re.findall(pattern,sel.name())
                            if buf:
                                start = int(buf[0][0])
                                end = int(buf[0][1])
                                pm.delete('%s.f[%s:%s]' % (sel.node().name(),start,end))
                        else:
                            try:
                                faceCount = sel.numFaces()
                                if faceCount>1:
                                    pm.delete('%s.f[1:%s]' % (sel.name(),faceCount))
                            except:
                                logger.warning('"%s" do use face delete faild.' % sel.name())
                        try:
                            pm.select(sel,r=True)
                            mel.eval('doBakeNonDefHistory( 1, {"prePost" })')
                        except:pass

        endTime = time.time()
        logger.debug(format('Delete not use face of meshes.',' <120')+'%.2f s' % (endTime-startTime))

    @staticmethod
    def replaceAttribute(nodeType,nodeAttr,sourceStr,replaceStr,*args):
        nodes = pm.ls(type=nodeType)
        for node in nodes:
            attrValue = node.attr(nodeAttr).get().replace(sourceStr,replaceStr)
            node.attr(nodeAttr).set(attrValue)

    @staticmethod
    def openWithMaya(self):
        pass
        # open maya file, and replace attribute
        # try:
        #     if os.path.isfile(sourceFilePath):
        #         pm.openFile(sourceFilePath,force=True)
        # except:
        #     import upUtil
        #     errorInfo=upUtil.traceException(makeError=0)
        #     logger.error('\t%s' % errorInfo)
        # endTime = time.time()
        # logger.debug(format('Opening file',' <120')+' %.2f s' % (endTime-startTime))

        # replace_attribute_mode = self.ui.replace_attribute_mode_comboBox.currentText().split('-->')
        # Core.replaceAttribute(nodeType='file',nodeAttr='fileTextureName',
        #     sourceStr=replace_attribute_mode[0].strip(),
        #     replaceStr=replace_attribute_mode[1].strip())

        # pm.renameFile(targetFilePath)
        # pm.saveFile(force=True)
        # pm.newFile(force=True)

    @staticmethod
    def adjustFileTimes(sourceFilePath,targetFilePath):
        atime = os.path.getatime( sourceFilePath )
        mtime = os.path.getmtime( sourceFilePath )
        os.utime(targetFilePath, (atime,mtime))

    @staticmethod
    def replaceAttributeWithFile(sourceFilePath,targetFilePath,sourceStr,replaceStr,*args):
        if not os.path.isfile(sourceFilePath):
            return False

        status = True

        startTime = time.time()

        with codecs.open(sourceFilePath,'r','gb2312') as sourceFile:
            with codecs.open(targetFilePath,'w','gb2312') as targetFile:
                line = sourceFile.readline()
                i = 1
                while line:
                    try:
                        line = line.replace(sourceStr,replaceStr)
                    except:
                        logger.error('%s:%s' % (i,line))
                        status = False
                        break

                    i += 1
                    targetFile.write(line)
                    line = sourceFile.readline()

        if not status:
            if os.path.exists(targetFilePath):
                try:
                    os.remove(targetFilePath)
                except OSError, e:
                    logger.error("%s - %s." % (e.filename,e.strerror))
            else:
                logger.debug("Sorry, I can not find %s file." % targetFilePath)
            return False

        endTime = time.time()
        logger.debug(format('Replace attributes:',' <120')+'%.2f s' % (endTime-startTime))
        return True