#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date     : 3/1/2018 10:15 AM
# @Author   : Godfrey Huang

import sys, os
import math
import random
import re
import time
import glob
from functools import partial
import subprocess
import shutil
import json
from collections import OrderedDict
from Qt import QtCore, QtGui, QtWidgets, IsPySide, IsPySide2

import core
reload(core)
from core import Core

import logging
from log import getLogger

logger = getLogger('holyMayaFileEditor')
logger.setLevel(logging.DEBUG)


_window_object_name = 'holyMayaFileEditor'
parent = None

if IsPySide:
    import ui.mainWindow_ui as mainWindow_ui
    reload(mainWindow_ui)
    import ui.dialog_ui as dialog_ui
    reload(dialog_ui)
    import ui.progress_ui as progress_ui
    reload(progress_ui)
    from shiboken import wrapInstance

elif IsPySide2:
    import ui.mainWindow_pyside2ui as mainWindow_ui
    reload(mainWindow_ui)
    import ui.dialog_pyside2ui as dialog_ui
    reload(dialog_ui)
    import ui.progress_pyside2ui as progress_ui
    reload(progress_ui)
    from shiboken2 import wrapInstance

import ui.outputLog_ui as outputLog_ui
reload(outputLog_ui)

try:
    import maya.OpenMayaUI as omui
    import maya.OpenMaya as om
    import pymel.core as pm
    import pymel.core.datatypes as dt
    import maya.cmds as cmds
    import maya.mel as mel
    import uploader
    reload(uploader)
    from uploader import Uploader

    def getMayaWindow():
        ptr = omui.MQtUtil.mainWindow()
        return wrapInstance(long(ptr), QtWidgets.QWidget)

    parent = getMayaWindow()
except:
    pass

class ProgressDialog(QtWidgets.QDialog):
    def __init__(self,parent=None):
        super(ProgressDialog,self).__init__(parent)
        self.ui = progress_ui.Ui_Dialog()
        self.ui.setupUi(self)
        self.setDocumentMode(False)
        self.setDockOptions(QtWidgets.QDialog.AllowTabbedDocks | QtWidgets.QDialog.AnimatedDocks)
        self.setUnifiedTitleAndToolBarOnMac(False)
        self.setWindowFlags(self.windowFlags() | QtCore.Qt.WindowStaysOnTopHint)
        self.connections()

    def connections(self):
        self.ui.progressBar.valueChanged.connect(self.closeWindow)

    def setValue(self,value):
        self.ui.progressBar.setValue(value)

    def setMaximum(self,max):
        self.ui.progressBar.setMaximum(max)

    def value(self):
        return self.ui.progressBar.value()

    def closeWindow(self,value):
        if value == self.ui.progressBar.maximum():
            self.close()

class FileSystemModel(QtWidgets.QFileSystemModel):
    INIT_MODE = -1
    OUTPUT_MODE = 0
    REPLACE_ATTRIBUTE_MODE = 1
    SEQUENCE_CHECK_COPY_MODE = 2
    SOURCE_MODE = 3
    TARGET_MODE = 4

    def __init__(self,sourceOrTarget,parent=None):
        QtWidgets.QFileSystemModel.__init__(self,parent)
        self.mode = self.INIT_MODE
        self.sourceOrTarget = sourceOrTarget

    def setMode(self,mode):
        self.mode = mode

    def data(self, index, role=QtCore.Qt.DisplayRole):
        if not index.isValid():
            return QtWidgets.QFileSystemModel.data(self, index, role)

        if self.sourceOrTarget == self.SOURCE_MODE:
            if index.column() == 0:
                if role == QtCore.Qt.BackgroundColorRole:
                    sourceFilePath = self.filePath(index)
                    if self.mode == self.OUTPUT_MODE:
                        if os.path.isfile(sourceFilePath):
                            buf = os.path.basename(sourceFilePath).split('.')
                            targetFilePath = os.path.join(os.path.dirname(sourceFilePath),'.'.join((buf[0]+'_mat',buf[1])))
                            if os.path.isfile(targetFilePath):
                                if os.stat(sourceFilePath).st_mtime > os.stat(targetFilePath).st_mtime:
                                    return QtGui.QColor(0,0,255,255)
                                if int(os.path.getsize(targetFilePath))/(1024.0*1024.0)>10.0:
                                    return QtGui.QColor(255,255,0,128)
                                return QtGui.QColor(0,255,0,128)
                            else:
                                return QtGui.QColor(255,0,0,50)

                    if self.mode == self.SEQUENCE_CHECK_COPY_MODE:
                        if os.path.isdir(sourceFilePath):
                            parentData = index.parent().data(QtCore.Qt.DisplayRole)
                            if parentData:
                                pattern = re.compile(parentData)
                                if pattern.findall(self.rootPath()):
                                    state = None
                                    contents = glob.glob(os.path.join(sourceFilePath,'*.exr'))
                                    if contents:
                                        state = 'ok'
                                    else:
                                        state = '404'

                                    if state == '404':
                                        return QtGui.QColor(255,0,0,50)

        return QtWidgets.QFileSystemModel.data(self, index, role)

class MainWindow(QtWidgets.QMainWindow):
    '''
    holy maya file editor main window
    '''
    HOLY_ID = 0
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.parent = parent
        self.ui = mainWindow_ui.Ui_MainWindow()
        self.ui.setupUi(self)
        self.setObjectName(_window_object_name)
        self.setDocumentMode(False)
        self.setDockOptions(QtWidgets.QMainWindow.AllowTabbedDocks | QtWidgets.QMainWindow.AnimatedDocks)
        self.setUnifiedTitleAndToolBarOnMac(False)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        # self.setWindowFlags(QtCore.Qt.Tool)
        # self.setWindowFlags(self.windowFlags() | QtCore.Qt.WindowStaysOnTopHint)
        cssPath = os.path.join(os.path.dirname(os.path.realpath(__file__)),'style.css')
        with open(cssPath) as cssFile:
            styleData = cssFile.read()
        self.setStyleSheet(styleData)

        self.initData()
        self.createContent()
        self.settings()
        self.connections()
        self.readSettings()

    def refreshCallCount(self):
        # HOLY_ID = 9660
        if self.HOLY_ID == 0:
            return

        try:
            import HolyCmdLog
            HolyCmdLog.saveusercmdlogtofile(self.HOLY_ID)
        except:pass

    def initData(self):
        self.source_regular = ''
        self.sourceFilesModel = FileSystemModel(sourceOrTarget=FileSystemModel.SOURCE_MODE)
        self.sourceFilesModel.setFilter(
            QtCore.QDir.NoSymLinks |
            QtCore.QDir.NoDotAndDotDot |
            QtCore.QDir.Dirs |
            QtCore.QDir.Files
            )
        self.sourceFilesModel.setReadOnly(True)
        self.ui.source_files_treeView.setModel(self.sourceFilesModel)
        self.sourceSelectModel = self.ui.source_files_treeView.selectionModel()

        self.targetFilesModel = FileSystemModel(sourceOrTarget=FileSystemModel.TARGET_MODE)
        self.targetFilesModel.setFilter(
            QtCore.QDir.NoSymLinks |
            QtCore.QDir.NoDotAndDotDot |
            QtCore.QDir.Dirs |
            QtCore.QDir.Files
            )
        self.targetFilesModel.setReadOnly(True)
        self.ui.target_files_treeView.setModel(self.targetFilesModel)
        self.targetSelectModel = self.ui.target_files_treeView.selectionModel()
        self.filedsOrderedDict = OrderedDict([('name',True),('size',True),('type',True),('date',True)])
        self.treeViewRightKeyMenuActions = OrderedDict(
                [
                    ('Open in Maya',partial(Core.openInApp,Core.MAYA)),
                    ('Reveal in Explorer',partial(Core.openInApp,Core.EEXPLORER))
                ]
            )
        self.dataSettings = QtCore.QSettings("mayaFileEditor", "mayaFileEditorWindow")

    # ui function
    def createContent(self):
        self.ui.outputDialog = outputLog_ui.OutputLogDialog(self)

        def createFiledsMenu(widget,header):
            menu = QtWidgets.QMenu(widget)
            i = 0
            for name, checked in self.filedsOrderedDict.iteritems():
                action = menu.addAction(name)
                action.setCheckable(True)
                action.triggered.connect(partial(self.filedsMenuCallback,action,i,header))
                action.setChecked(checked)
                i += 1
            widget.setMenu(menu)

        createFiledsMenu(self.ui.source_fileds_toolButton,self.ui.source_files_treeView.header())
        createFiledsMenu(self.ui.target_fileds_toolButton,self.ui.target_files_treeView.header())

    def settings(self):
        self.ui.source_files_treeView.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.ui.source_files_treeView.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.ui.source_files_treeView.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        header = self.ui.source_files_treeView.header()
        try:
            header.setResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        except:
            header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)

        #self.ui.target_files_treeView.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.ui.target_files_treeView.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.ui.target_files_treeView.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        header = self.ui.target_files_treeView.header()
        try:
            header.setResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        except:
            header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)

        self.ui.output_directory_widget.setHidden(True)
        self.ui.upload_checkBox.setHidden(True)
        self.ui.replace_attribute_mode_widget.setHidden(True)
        self.ui.output_json_setting_widget.setHidden(True)
        self.ui.output_json_output_directory_widget.setHidden(True)
        self.ui.output_json_regular_widget.setHidden(True)
        self.ui.traverse_folder_checkBox.setHidden(True)
        self.ui.source_files_treeView.setSortingEnabled(True)
        self.ui.source_files_treeView.sortByColumn(0, QtCore.Qt.AscendingOrder)
        self.ui.target_files_treeView.setSortingEnabled(True)
        self.ui.target_files_treeView.sortByColumn(0, QtCore.Qt.AscendingOrder)

    def connections(self):
        self.ui.actionAbout.triggered.connect(self.aboutActionCallback)
        self.ui.browse_pushButton.clicked.connect(partial(self.browseCallback,self.ui.directory_comboBox))
        self.ui.output_directory_browse_pushButton.clicked.connect(partial(self.browseCallback,self.ui.output_directory_comboBox))
        self.ui.source_search_pushButton.clicked.connect(self.updateSourceTreeView)
        self.ui.target_filter_pushButton.clicked.connect(self.updateTargetTreeView)
        self.sourceFilesModel.directoryLoaded.connect(partial(self.treeViewFetchAndExpand,self.ui.source_files_treeView,self.ui.source_items_label))
        self.targetFilesModel.directoryLoaded.connect(partial(self.treeViewFetchAndExpand,self.ui.target_files_treeView,self.ui.target_items_label))
        self.ui.source_files_treeView.customContextMenuRequested.connect(partial(self.treeViewPopupMenus,self.ui.source_files_treeView))
        self.ui.target_files_treeView.customContextMenuRequested.connect(partial(self.treeViewPopupMenus,self.ui.target_files_treeView))
        self.ui.callback_comboBox.currentIndexChanged.connect(self.comboBoxIndexChangeCallback)
        self.ui.source_files_treeView.clicked.connect(partial(self.treeViewClickedCallback,self.ui.source_files_treeView,self.ui.source_select_state_label))
        self.ui.batch_pushButton.clicked.connect(self.batch)
        self.ui.log_pushButton.clicked.connect(self.showOutoutLogDialog)
        self.ui.output_directory_comboBox.currentIndexChanged.connect(self.outputComboBoxCallback)

    # ui callbacks
    def outputComboBoxCallback(self,index,*args):
        outputDir = self.ui.output_directory_comboBox.itemText(index)
        if outputDir in ['//nas/outsource/nezha',]:
            self.ui.upload_checkBox.setChecked(True)
        else:
            self.ui.upload_checkBox.setChecked(False)

    def showOutoutLogDialog(self):
        self.ui.outputDialog.show()

    def browseCallback(self,comboBox):
        options = QtWidgets.QFileDialog.Options(QtWidgets.QFileDialog.ShowDirsOnly)
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        directory = QtWidgets.QFileDialog.getExistingDirectory(self, "Select a directory",
                comboBox.currentText() or QtCore.QDir.currentPath(),
                options
                )

        if directory:
            if comboBox.findText(directory) == -1:
                comboBox.addItem(directory)

            comboBox.setCurrentIndex(comboBox.findText(directory))

    def filedsMenuCallback(self,action,index,header,*args):
        state = action.isChecked()
        if state:
            header.showSection(index)
        else:
            header.hideSection(index)

    def treeViewClickedCallback(self,view,selectStateLabel,index,*args):
        model = view.model()
        filePath = model.filePath(index)
        selectStateLabel.setText('')

        if model.mode == FileSystemModel.SEQUENCE_CHECK_COPY_MODE:
            contents = glob.glob(os.path.join(filePath,'*.exr'))
            if not contents:
                pm.warning('No exr file detected, Cannot excute any operation!')
                return
            # update source select state label text
            selectStateLabel.setText('%i item exr image' % len(contents))

            # update target tree view selection item
            outputDir = self.ui.output_directory_comboBox.currentText()
            if not outputDir or outputDir == self.ui.directory_comboBox.currentText():
                pm.warning('Please select a valid output directory!')
                return

    def switchOrAddItemOfComboBox(self,comboBox,itemText):
        # items = [comboBox.itemText(i) for i in range(comboBox.count())]
        # if items:
        #     count = len(items)
        index = comboBox.findText(itemText)
        if index != -1:
            comboBox.setCurrentIndex(index)
        else:
            comboBox.addItem(itemText)
            index = comboBox.findText(itemText)
            comboBox.setCurrentIndex(index)

    def comboBoxIndexChangeCallback(self,index):
        self.ui.batch_pushButton.setText('Batch')
        if index == 1: #'output material'
            self.sourceFilesModel.setMode(FileSystemModel.OUTPUT_MODE)
            self.targetFilesModel.setMode(FileSystemModel.OUTPUT_MODE)
            self.ui.output_directory_widget.setHidden(True)
            self.ui.upload_checkBox.setHidden(True)
            self.ui.replace_attribute_mode_widget.setHidden(True)
            self.ui.output_json_setting_widget.setHidden(False)
            self.ui.level_comboBox.setCurrentIndex(1)
            self.ui.traverse_folder_checkBox.setHidden(False)
            self.ui.traverse_folder_checkBox.setChecked(False)
            self.ui.callback_tip_info_label.setText('')
            self.switchOrAddItemOfComboBox(self.ui.source_regular_comboBox,'[a-zA-Z0-9_]*_tex.ma')
            self.switchOrAddItemOfComboBox(self.ui.target_regular_comboBox,'[a-zA-Z0-9_]*_tex_mat.ma')
        elif index == 2: #'replace attribute'
            self.HOLY_ID = 23836
            self.refreshCallCount()
            self.sourceFilesModel.setMode(FileSystemModel.REPLACE_ATTRIBUTE_MODE)
            self.targetFilesModel.setMode(FileSystemModel.REPLACE_ATTRIBUTE_MODE)
            self.ui.output_directory_widget.setHidden(False)
            self.ui.upload_checkBox.setHidden(False)
            self.ui.replace_attribute_mode_widget.setHidden(False)
            self.ui.output_json_setting_widget.setHidden(True)
            self.ui.traverse_folder_checkBox.setHidden(False)
            self.ui.level_comboBox.setCurrentIndex(0)
            self.ui.traverse_folder_checkBox.setChecked(True)
            self.ui.callback_tip_info_label.setHidden(False)
            self.ui.callback_tip_info_label.setText(u'对文件夹进行批量操作，请选择所有左边的项，非ma/mb文件会自动拷贝到输出目录')
            self.ui.callback_tip_info_label.setStyleSheet('color:yellow')
            self.switchOrAddItemOfComboBox(self.ui.source_regular_comboBox,'[a-zA-Z0-9_]*')
            self.switchOrAddItemOfComboBox(self.ui.target_regular_comboBox,'[a-zA-Z0-9_]*')
        elif index == 3: #'sequence check and copy'
            self.HOLY_ID = 29977
            self.refreshCallCount()
            self.sourceFilesModel.setMode(FileSystemModel.SEQUENCE_CHECK_COPY_MODE)
            self.targetFilesModel.setMode(FileSystemModel.SEQUENCE_CHECK_COPY_MODE)
            self.ui.callback_tip_info_label.setHidden(False)
            self.ui.callback_tip_info_label.setText(u'对文件夹进行检查拷贝操作，请选择左边的项，所选文件夹会自动按场次镜头号拷贝到输出目录')
            self.ui.callback_tip_info_label.setStyleSheet('color:yellow')
            self.ui.output_directory_widget.setHidden(False)
            self.ui.upload_checkBox.setHidden(True)
            self.ui.replace_attribute_mode_widget.setHidden(True)
            self.ui.output_json_setting_widget.setHidden(True)
            self.ui.traverse_folder_checkBox.setHidden(False)
            self.ui.level_comboBox.setCurrentIndex(0)
            self.ui.traverse_folder_checkBox.setChecked(True)
            self.ui.batch_pushButton.setText('Copy')
            self.switchOrAddItemOfComboBox(self.ui.source_regular_comboBox,'[a-zA-Z0-9_]*')
            self.switchOrAddItemOfComboBox(self.ui.target_regular_comboBox,'[a-zA-Z0-9_]*')
        else:
            self.sourceFilesModel.setMode(FileSystemModel.INIT_MODE)
            self.targetFilesModel.setMode(FileSystemModel.INIT_MODE)
            self.ui.output_directory_widget.setHidden(True)
            self.ui.upload_checkBox.setHidden(True)
            self.ui.replace_attribute_mode_widget.setHidden(True)
            self.ui.output_json_setting_widget.setHidden(True)
            self.ui.traverse_folder_checkBox.setHidden(False)
            self.ui.level_comboBox.setCurrentIndex(0)
            self.ui.traverse_folder_checkBox.setChecked(False)
            self.ui.callback_tip_info_label.setText('')

    def aboutActionCallback(self):
        dialogWin = QtWidgets.QDialog(self)
        dialog = dialog_ui.Ui_Dialog()
        dialog.setupUi(dialogWin)
        dialogWin.show()

    def readSettings(self):
        self.dataSettings.beginGroup('maya_file_editor_mainWindow')
        windowGeometry = self.dataSettings.value('window_geometry')
        windowState = self.dataSettings.value('window_state')
        directoryBrowHistory = self.dataSettings.value('directory_brow_history')
        outputDirectoryBrowHistory = self.dataSettings.value('output_directory_brow_history')
        sourceRegularHistory = self.dataSettings.value('source_regular_history')
        self.restoreGeometry(windowGeometry)
        self.restoreState(windowState)
        if directoryBrowHistory:
            directoryCount = len(directoryBrowHistory)
            for i in range(directoryCount):
                if (directoryCount>10 and i>=(directoryCount-10)) or (directoryCount <= 10):
                    if self.ui.directory_comboBox.findText(directoryBrowHistory[i]) == -1:
                        # pass
                        self.ui.directory_comboBox.addItem(directoryBrowHistory[i])
            self.ui.directory_comboBox.setCurrentIndex(self.ui.directory_comboBox.count()-1)
        if outputDirectoryBrowHistory:
            directoryCount = len(outputDirectoryBrowHistory)
            for i in range(directoryCount):
                if (directoryCount>10 and i>=(directoryCount-10)) or (directoryCount <= 10):
                    if self.ui.output_directory_comboBox.findText(outputDirectoryBrowHistory[i]) == -1:
                        # pass
                        self.ui.output_directory_comboBox.addItem(outputDirectoryBrowHistory[i])
            self.ui.directory_comboBox.setCurrentIndex(self.ui.directory_comboBox.count()-1)
        if sourceRegularHistory:
            regularCount = len(sourceRegularHistory)
            for i in range(regularCount):
                if (regularCount>10 and i>=(regularCount-10)) or (regularCount <= 10):
                    if self.ui.source_regular_comboBox.findText(sourceRegularHistory[i]) == -1:
                        #pass
                        self.ui.source_regular_comboBox.addItem(sourceRegularHistory[i])
            self.ui.source_regular_comboBox.setCurrentIndex(self.ui.source_regular_comboBox.count()-1)
        self.dataSettings.endGroup()

    def writeSettings(self):
        self.dataSettings.beginGroup('maya_file_editor_mainWindow')
        self.dataSettings.setValue('window_geometry', self.saveGeometry())
        self.dataSettings.setValue('window_state', self.saveState())
        self.dataSettings.setValue('directory_brow_history', [self.ui.directory_comboBox.itemText(i) for i in range(self.ui.directory_comboBox.count())])
        self.dataSettings.setValue('output_directory_brow_history', [self.ui.output_directory_comboBox.itemText(i) for i in range(self.ui.output_directory_comboBox.count())])
        self.dataSettings.setValue('source_regular_history', [self.ui.source_regular_comboBox.itemText(i) for i in range(self.ui.source_regular_comboBox.count())])
        self.dataSettings.endGroup()

    def closeEvent(self,event):
        self.writeSettings()

        for handler in logger.handlers:
            if type(handler).__name__ == 'WidgetHandler':
                handler.clearWidgets()

    # functions
    def treeViewFetchAndExpand(self,view,itemsLabel,path):
        model = view.model()
        index = model.index(path)
        count = model.rowCount(index)

        if self.ui.level_comboBox.currentIndex() > 0:
            view.expand(index)

        if model.mode in (FileSystemModel.REPLACE_ATTRIBUTE_MODE,FileSystemModel.SEQUENCE_CHECK_COPY_MODE):
            rootIndex = model.index(self.ui.output_directory_comboBox.currentText())
        elif model.mode in (FileSystemModel.OUTPUT_MODE,):
            rootIndex = model.index(self.ui.directory_comboBox.currentText())
        else:
            rootIndex = model.index(self.ui.directory_comboBox.currentText())

        if path == model.filePath(rootIndex):
            itemsLabel.setText('%i items' % count)

        if model.mode in (FileSystemModel.OUTPUT_MODE,FileSystemModel.REPLACE_ATTRIBUTE_MODE,):
            for i in range(count):
                # fetch all the sub-folders
                child = index.child(i, 0)
                childFilePath = model.filePath(child)
                if model.isDir(child):
                    model.setRootPath(childFilePath)
                    model.setNameFilterDisables(self.ui.level_comboBox.currentIndex()>1)

    def treeViewPopupMenus(self,treeView,position):
        indexes = [index for index in treeView.selectedIndexes() if index.column()==0]

        if len(indexes) > 0:
            level = 0
            index = indexes[0]
            while index.parent().isValid():
                index = index.parent()
                level += 1

        menu = QtWidgets.QMenu()
        if level > 0:
            actionDict = OrderedDict(self.treeViewRightKeyMenuActions)
            treeViewModel = treeView.model()
            if treeViewModel.sourceOrTarget == FileSystemModel.SOURCE_MODE and treeViewModel.mode == FileSystemModel.SEQUENCE_CHECK_COPY_MODE:
                actionDict['Open in RV'] = partial(Core.openInApp,Core.RVPLAYER)

            for label, func in actionDict.iteritems():
                action = QtWidgets.QAction(self)
                action.setText(label)
                action.triggered.connect(partial(func,treeViewModel,indexes))
                menu.addAction(action)

        menu.exec_(treeView.viewport().mapToGlobal(position))

    # core function
    def updateSourceTreeView(self):
        text = self.ui.directory_comboBox.currentText()
        self.source_regular = self.ui.source_regular_comboBox.currentText()
        self.sourceFilesModel.setRootPath(text)
        self.sourceFilesModel.setNameFilters([self.source_regular])
        self.sourceRootIndex = self.sourceFilesModel.index(text)
        self.ui.source_files_treeView.setRootIndex(self.sourceRootIndex)

    def updateTargetTreeView(self):
        if self.targetFilesModel.mode in (FileSystemModel.REPLACE_ATTRIBUTE_MODE,FileSystemModel.SEQUENCE_CHECK_COPY_MODE):
            text = self.ui.output_directory_comboBox.currentText()
        else:
            text = self.ui.directory_comboBox.currentText()
        self.target_regular = self.ui.target_regular_comboBox.currentText()
        self.targetFilesModel.setRootPath(text)
        self.targetFilesModel.setNameFilters([self.target_regular])
        self.targetRootIndex = self.targetFilesModel.index(text)
        self.ui.target_files_treeView.setRootIndex(self.targetRootIndex)

    def batch(self):
        if self.sourceFilesModel.mode == FileSystemModel.INIT_MODE:
            pm.warning('Please select a callback!')
            return
        self.showOutoutLogDialog()
        selectedItems = self.sourceSelectModel.selectedRows(0)
        count = len(selectedItems)

        uploadFiles = []
        uploadLocalDirPath = ''
        for i in range(count):
            selectItem = selectedItems[i]
            sourceFilePath = self.sourceFilesModel.filePath(selectItem)

            if self.sourceFilesModel.mode == FileSystemModel.OUTPUT_MODE:
                if not os.path.isfile(sourceFilePath):
                    continue
                logger.info('process: %s/%s, source file: %s' % (i+1,count,sourceFilePath))
                sourceFileDirPath = os.path.dirname(sourceFilePath)

                fileName = '%s_mat.%s' % tuple(os.path.basename(sourceFilePath).split('.'))
                targetFilePath = os.path.join(sourceFileDirPath,fileName)

                Core.deleteRequirePlugin(sourceFilePath,targetFilePath)

                startTime = time.time()
                try:
                    if os.path.isfile(targetFilePath):
                        pm.openFile(targetFilePath,force=True)
                except:
                    import upUtil
                    errorInfo=upUtil.traceException(makeError=0)
                    logger.error('%s' % errorInfo)
                endTime = time.time()
                logger.debug(format('Opening file',' <120')+'%.2f s' % (endTime-startTime))

                Core.deleteAlembicNodes()
                Core.deleteNotUseFaceOfMeshs()

                pm.renameFile(targetFilePath)
                pm.saveFile(force=True)
                pm.newFile(force=True)
                logger.debug('Output material file: %s' % targetFilePath)

                if self.ui.output_json_file_checkBox.isChecked():
                    sourceFileDirPath = os.path.dirname(sourceFilePath)
                    fileName = '%s_mat.json' % tuple(os.path.basename(sourceFilePath).split('.'))[0]
                    replaceStr = self.ui.output_json_regular_source_lineEdit.text()
                    targetStr = self.ui.output_json_regular_target_lineEdit.text()
                    if replaceStr == targetStr:
                        continue
                    relativePath = self.ui.output_json_output_directory_lineEdit.text()
                    if not relativePath:
                        continue
                    fileName = fileName.replace(replaceStr,targetStr)
                    jsonFilePath = os.path.join(os.path.realpath(os.path.join(sourceFileDirPath,relativePath)),fileName)

                    dataDict = {'mat_file':targetFilePath}
                    with open(jsonFilePath,'w') as f:
                        json.dump(dataDict,f,indent=4)

                    if os.path.isfile(jsonFilePath):
                        logger.debug('Output json file: %s' % jsonFilePath)

            if self.sourceFilesModel.mode == FileSystemModel.REPLACE_ATTRIBUTE_MODE:
                if not self.ui.output_directory_comboBox.currentText():
                    pm.warning('Please select a output directory!')
                    continue

                def copyFilePath(sourceFilePath,targetTempDir=None,uploaded=False,*args):
                    targetDirPath = targetTempDir if uploaded else self.ui.output_directory_comboBox.currentText()
                    targetFilePath = sourceFilePath.replace(self.ui.directory_comboBox.currentText(),targetDirPath)
                    if os.path.isfile(sourceFilePath):
                        uploadFiles.append(targetFilePath.replace(targetDirPath,''))
                        splitTextBuffer = os.path.splitext(sourceFilePath)
                        if splitTextBuffer:
                            fileExt = splitTextBuffer[1]
                            targetDir = os.path.dirname(targetFilePath)
                            if not os.path.exists(targetDir):
                                os.makedirs(targetDir)

                            if fileExt in ('.ma',):
                                logger.info('process: %s/%s, source file: %s' % (i+1,count,sourceFilePath))
                                # read/write file, and replace attribute
                                replace_attribute_mode = self.ui.replace_attribute_mode_comboBox.currentText().split('-->')
                                status = Core.replaceAttributeWithFile(sourceFilePath,targetFilePath,
                                                            replace_attribute_mode[0].strip(),
                                                            replace_attribute_mode[1].strip())
                                Core.adjustFileTimes(sourceFilePath,targetFilePath)
                                if status:
                                    logger.debug('Output target file: %s' % targetFilePath)
                                else:
                                    logger.error('Cannot output target file: %s' % targetFilePath)

                            else:
                                shutil.copy2(sourceFilePath,targetFilePath)
                                logger.debug('"%s" copied successfully!' % targetFilePath)

                    if os.path.isdir(sourceFilePath):
                        if not os.path.exists(targetFilePath):
                            if self.ui.traverse_folder_checkBox.isChecked():
                                os.mkdir(targetFilePath)
                                logger.debug('"%s" folder has been created!' % targetFilePath)

                                for child in os.listdir(sourceFilePath):
                                    sourceChildFilePath = os.path.join(sourceFilePath,child)
                                    copyFilePath(sourceChildFilePath,targetTempDir,uploaded)
                            else:
                                try:
                                    os.makedirs(targetFilePath)
                                    logger.debug('"%s" folder has been created!' % targetFilePath)
                                except:
                                    logger.error('"%s" folder failed to create, please make sure you have permission to create folder!' % targetFilePath)

                if self.ui.upload_checkBox.isChecked():
                    systemTempPath = os.environ['TMP']
                    timestamps = time.strftime('%Y%m%d%H%M%S',time.localtime(time.time()))
                    uploadLocalDirPath = os.path.join(systemTempPath,'upload%s' % timestamps)
                    if not os.path.exists(uploadLocalDirPath):
                        os.makedirs(uploadLocalDirPath)
                    copyFilePath(sourceFilePath,uploadLocalDirPath,True)
                else:
                    copyFilePath(sourceFilePath)

            if self.sourceFilesModel.mode == FileSystemModel.SEQUENCE_CHECK_COPY_MODE:
                def getSeqCopyOutputDirPath(filePath,outputDir):
                    pattern = re.compile('[a-zA-Z0-9_]+(seq[0-9]+)_(shot[0-9]+)[a-zA-Z0-9_.]+')
                    seqShotBuf = pattern.findall(os.path.basename(filePath))
                    if len(seqShotBuf[0]) == 2:
                        seq = seqShotBuf[0][0]
                        shot = seqShotBuf[0][1]
                        copyDirPath = os.path.join(outputDir,seq,seq+'_'+shot)
                        return copyDirPath
                    return None

                sourceDirName = os.path.basename(sourceFilePath)
                outputDirPath = self.ui.output_directory_comboBox.currentText()
                seqOutputDirParentPath = getSeqCopyOutputDirPath(sourceFilePath,outputDirPath)
                seqOutputDirPath = os.path.join(seqOutputDirParentPath,sourceDirName)
                if not os.path.exists(seqOutputDirPath):
                    os.makedirs(seqOutputDirPath)

                exrFiles = glob.glob(os.path.join(sourceFilePath,'*.exr'))
                for exr in exrFiles:
                    exrBaseName = os.path.basename(exr)
                    distExrFile = os.path.join(seqOutputDirPath,exrBaseName)
                    shutil.copyfile(exr,distExrFile)
                logger.info('Copied %s exr files to "%s"' % (len(exrFiles),seqOutputDirPath))

        if self.sourceFilesModel.mode == FileSystemModel.REPLACE_ATTRIBUTE_MODE:
            if self.ui.upload_checkBox.isChecked():
                logger.debug('upload files, count: %s' % len(uploadFiles))
                uploader = Uploader(localDir=uploadLocalDirPath,
                                    uploadDir=self.ui.output_directory_comboBox.currentText(),
                                    files=uploadFiles)
                uploader.do()
                logger.debug('upload files finished!')

        logger.info('Congratulation, Batch processing is completed!\n')
        # self.updateTargetTreeView()

def main():
    '''
    holy maya file editor
    '''
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication([])

    for widget in app.topLevelWidgets():
        if widget.objectName() == _window_object_name:
            widget.close()
            widget.deleteLater()

    window = MainWindow(parent=parent)
    window.show()

    window.raise_()
    try:
        sys.exit(app.exec_())
    except: pass

if __name__ == "__main__":
    main()